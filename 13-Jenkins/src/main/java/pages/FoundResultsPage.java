package pages;

import com.codeborne.selenide.SelenideElement;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$$;

public class FoundResultsPage {

    private List<SelenideElement> foundItems = $$("a > h3");

    public List<String> getFoundItemsTitles() {
        return foundItems.stream().map(i -> i.getText()).collect(Collectors.toList());
    }
}
