package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selectors.byName;

public class GoogleSearchPage {

    private SelenideElement searchField = $(byName("q"));

    private SelenideElement submitButton = $(byName("btnK"));

    public void performSearch(String searchText) {
        searchField.sendKeys(searchText);
        submitButton.click();
    }
}
