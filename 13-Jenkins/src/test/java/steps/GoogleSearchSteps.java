package steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static com.codeborne.selenide.Selenide.open;
import static junit.framework.TestCase.assertTrue;

public class GoogleSearchSteps extends BeforeGoogleSearchScenario {

    @Given("I navigate to Google search page")
    public void navigateToGoogleSearchPage() {
        open("https://www.google.com/");
    }

    @When("User searches for $searchPhrase")
    public void searchFor(String searchPhrase) {
        phrase = searchPhrase;
        searchPage.performSearch(searchPhrase);
    }

    @Then("Each found item on results page contains search phrase")
    public void checkFoundItems() {
        resultsPage.getFoundItemsTitles().stream().forEach(i -> assertTrue(i.contains(phrase)));
    }
}
