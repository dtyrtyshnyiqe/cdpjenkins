package steps;

import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;
import org.jbehave.core.steps.Steps;
import pages.FoundResultsPage;
import pages.GoogleSearchPage;

import static com.codeborne.selenide.Selenide.close;

public class BeforeGoogleSearchScenario extends Steps {

    BeforeGoogleSearchScenario() {
    }

    GoogleSearchPage searchPage;

    FoundResultsPage resultsPage;

    static String phrase;

    @BeforeStories
    public void setUp() {
        setUpBrowser();
        setPages();
    }

    private void setUpBrowser() {
        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
    }

    private void setPages() {
        searchPage = new GoogleSearchPage();
        resultsPage = new FoundResultsPage();
    }

    @AfterStories
    public void tearDown() {
        close();
    }
}
