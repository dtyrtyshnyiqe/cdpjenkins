Narrative:
As a user
I want to search for information in Google
So that I can find the searched information displayed on found results page

Scenario: 01 Search for text in Google
Given I navigate to Google search page
When User searches for EPAM
Then Each found item on results page contains search phrase